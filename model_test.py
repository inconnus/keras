from __future__ import print_function

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.datasets import imdb
import pandas
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils

import numpy as np
import math
from keras.models import load_model
import function as fn


dataframe = pandas.read_csv("data.csv", header=None)
dataset = dataframe.values


label_encoder = LabelEncoder()
label_encoder.fit([x[1] for x in dataset] )
label_encoded = label_encoder.transform([x[1] for x in dataset] )
dummy_y = np_utils.to_categorical(label_encoded)
while True:
    inp = input('Input:')
    data_encoder = LabelEncoder()

    data_encoded = fn.DataEncoder(data_encoder,dataset,[inp])
    #print(data_encoded)

    x_test = sequence.pad_sequences(data_encoded, maxlen=10)
    #print(data_encoded)

    model = load_model('my_model2.h5')
    predictions = model.predict(x_test)
    print(predictions)
    for x in range(len(predictions)):
        if predictions[x] < 0.5:
            predictions[x] = math.floor(predictions[x])
        else:
            predictions[x] = math.ceil(predictions[x])
    #print(np.reshape(predictions,1))
    print(label_encoder.inverse_transform(int(predictions)))
