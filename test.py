from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Embedding
from keras.layers import LSTM
from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import model_from_json
from matplotlib import pyplot
import numpy
numpy.set_printoptions(threshold=numpy.nan)
# Parameters for MNIST dataset
img_rows, img_cols = 28, 28
nb_classes = 10


# Parameters for LSTM network
nb_lstm_outputs = 30
nb_time_steps = img_rows
dim_input_vector = img_cols

(X_train, y_train), (X_test, y_test) = mnist.load_data()
print(len(X_test))
input_shape = (nb_time_steps, dim_input_vector)

print(y_test)

X_train = X_train.astype('float32') / 255.
X_test = X_test.astype('float32') / 255.
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)


print(len(X_train))
for i in range(0, 9):
	pyplot.subplot(330 + 1 + i)
	pyplot.imshow(X_test[i], cmap=pyplot.get_cmap('gray'))
# show the plot
pyplot.show()

model = Sequential()
model.add(LSTM(nb_lstm_outputs, input_shape=input_shape))
model.add(Dense(nb_classes, activation='softmax'))
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()

model.fit(X_train, Y_train, batch_size=16, epochs=10)
# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")
#score = model.evaluate(X_test, Y_test, batch_size=16)

json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")
 
# evaluate loaded model on test data
loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

score = loaded_model.evaluate(X_test, Y_test, verbose=0)
print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1]*100))



predictions = model.predict_classes(X_test )
print(predictions)
