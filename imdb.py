'''Trains an LSTM model on the IMDB sentiment classification task.
The dataset is actually too small for LSTM to be of any advantage
compared to simpler, much faster methods such as TF-IDF + LogReg.
# Notes
- RNNs are tricky. Choice of batch size is important,
choice of loss and optimizer is critical, etc.
Some configurations won't converge.
- LSTM loss decrease patterns during training can be quite different
from what you see with CNNs/MLPs/etc.
'''
from __future__ import print_function

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.datasets import imdb
import pandas
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils
from pythainlp.tokenize import word_tokenize
import numpy as np
import math
from keras.models import load_model


max_features = 20000
maxlen = 10  # cut texts after this number of words (among top max_features most common words)
batch_size = 32

print('Loading data...')
(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)



dataframe = pandas.read_csv("data.csv", header=None)
dataset = dataframe.values

encoder = LabelEncoder()
encoder.fit([x[1] for x in dataset] )
encoded_Y = encoder.transform([x[1] for x in dataset] )
dummy_y = np_utils.to_categorical(encoded_Y)


bar = [word_tokenize(x[0],engine='deepcut') for x in dataset ]
bar_dimension = [len(x) for x in bar]
encoder2 = LabelEncoder()
encoder2.fit([j for i in bar for j in i])
encoded_Y2 = encoder2.transform([j for i in bar for j in i] )
text_data = []
for x in range(len(bar)):
    temp_data = []
    for i in range(bar_dimension[x]):
        temp_data.append(encoded_Y2[0])
        encoded_Y2 = np.delete(encoded_Y2,0)
    text_data.append(temp_data)



print(text_data)
print(len(x_train), 'x sequences')
print(len(y_train), 'y sequences')

print('Pad sequences (samples x time)')
x_train = sequence.pad_sequences(text_data, maxlen=maxlen)
x_test = sequence.pad_sequences(text_data, maxlen=maxlen)
print('x_train shape:', x_train.shape)
print('x_test shape:', x_test.shape)

print('Build model...')
model = Sequential()
model.add(Embedding(max_features, 128))
model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(1, activation='sigmoid'))

# try using different optimizers and different optimizer configs
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

print('Train...')
print(x_train)
model.fit(x_train, encoded_Y,
          batch_size=batch_size,
          epochs=50,
          validation_data=(x_test, encoded_Y))
score, acc = model.evaluate(x_test, encoded_Y,
                            batch_size=batch_size)

model.save('my_model2.h5')
model = load_model('my_model2.h5')
print(encoded_Y,'y')
print('Test score:', score)
print('Test accuracy:', acc)
predictions = model.predict(x_test)
for x in range(len(predictions)):
    if predictions[x] < 0.5:
        predictions[x] = math.floor(predictions[x])
    else:
        predictions[x] = math.ceil(predictions[x])
print(predictions)
print(encoder.inverse_transform(predictions))
