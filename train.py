
from pythainlp.tokenize import word_tokenize
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Embedding
from keras.layers import LSTM
import numpy as np
import pandas
import os
from numpy import ndarray

dataframe = pandas.read_csv("data.csv", header=None)
dataset = dataframe.values

encoder = LabelEncoder()
encoder.fit([x[1] for x in dataset] )
encoded_Y = encoder.transform([x[1] for x in dataset] )
dummy_y = np_utils.to_categorical(encoded_Y)


bar = [word_tokenize(x[0],engine='newmm') for x in dataset ]
bar_dimension = [len(x) for x in bar]
encoder2 = LabelEncoder()
encoder2.fit([j for i in bar for j in i])
encoded_Y2 = encoder2.transform([j for i in bar for j in i] )
text_data = []
for x in range(len(bar)):
    temp_data = []
    for i in range(bar_dimension[x]):
        temp_data.append(encoded_Y2[0])
        encoded_Y2 = np.delete(encoded_Y2,0)
    text_data.append(temp_data)

'''
print( bar_dimension)

#print([j for i in bar for j in i])
#print(encoded_Y2)
#print(encoder.inverse_transform(0))
#print(np.shape(bar))
'''

print(bar)
print(encoded_Y2)
print(text_data)

x_train = sequence.pad_sequences(encoded_Y2, maxlen=80)

model = Sequential()
model.add(Embedding(2000, 128))
model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(1, activation='sigmoid'))

# try using different optimizers and different optimizer configs
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model.fit(x_train, encoded_Y,
          batch_size=32,
          epochs=15)
